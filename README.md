# Donjon rusher programmation web INSA 2021 

Projet de programmation web S1 2021-2022 INSA MIC
En collaboration avec Aymeric, Gabriel & Loïc


# A LIRE IMPERATIVEMENT

Merci de jouer a notre jeu !

# Controles : 
- Valider = Entrée
- Mute = M
- Ouvrir la boutique = B
- Quitter la boutique = Echap
- Ouvrir le coffre = C
- Naviguer dans les menus / sur la map = Flèches

# Pour lancer le jeu :
- Nouveau terminal dans le dossier principal 
- créer un serveur local 
- ex. avec python : 
- python3 -m http.server
- ouvrir un navigateur et aller sur : localhost:8000

Le jeu comporte un étage de 3x3 salles, n'hésitez pas à relancer la partie plusieurs fois si vous trouvez directement la sortie.
Un étage comporte au moins :
- 1 monstre
- 1 boutique
- 1 coffre
